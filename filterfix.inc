<?php
/**
 * @author Ron House http://drupal.org/user/171604
 * @file
 * A pair of filters to remove and then replace material that breaks other filters.
 */

/**
 * Either remove and store, or replace stored, text between marker tags.
 *
 * @param $text
 *  String. The text to be scanned
 * @param $delta
 *  Integer. Which operation: even==remove, odd==replace text stored by filter $delta - 1
 * @return
 *  String. The processed text.
 */
function _filterfix_process_text($text, $filter_num) {

  static $settings;
  if (empty($settings)) {
    $settings = variable_get('filterfix_settings', array());
  }
  if (! isset($settings['patterns'])) {
    return $text;
  }

  global $_filterfix_stored_bits;
  if (! is_array($_filterfix_stored_bits)) {
    $_filterfix_stored_bits = array();
  }
  $count = -1;

  if (($filter_num % 2) == 0) {
    // These filter(s) remove the problematic text and put a marker in its place

    foreach ($settings['patterns'] as $ndx => $pattern) {
      $pat = $pattern['regex'];
      $intext = $text;
      if (preg_match_all($pat, $text, $matches, PREG_SET_ORDER)) {
        // For each of the instances, remove the contents of the tag.

        //$filterfix_test = $text;
        foreach ($matches as $match) {
          $token = _filterfix_make_unique_token($text);
          $_filterfix_stored_bits[$token] = $match[$pattern['subpat']];
          $text = preg_replace(
            $pat,
            $token,
            $text,
            1
          );
        }
      }
    }
  }
  else {
    // These filter(s) restore the problematic text where the marker is found

    foreach ($_filterfix_stored_bits as $token => $original_text) {
      $text = str_replace(
        $token,
        $original_text,
        $text,
        $count
      );
      if ($count == 1) {
        unset($_filterfix_stored_bits[$token]);
      }
    }
  }
  return $text;
}

function _filterfix_make_unique_token(&$text) {
  static $token_count = 0;
  do {
    ++$token_count;
    $token = 'Zaq' . $token_count . 'qaZ';
  } while (strpos($text, $token) !== FALSE);
  return $token;
}
