Filterfix
=========

Filterfix is a simple module intended to get over one specific problem:
conflicting text filters. If you find that you need two text filters and
each one interferes with the other, so that there is no way to use them in
the same text format, then this module might be useful. It could also be
used to fix up the behavior of just one filter if you knew that it
malfunctioned on certain specific bits of text, but this writer doesn't
know of any specific cases of that.

Example:
--------

    The writer found that the Slideshow Creator filter was not working well
    with the Drupal 7 version of the "Convert line breaks into HTML"
    filter. The problem was: If Slideshow Creator was listed second, then
    you couldn't put any newlines into the filter's control block, because
    the Convert filter turned them into paragraph tags and broke the
    control block. On the other hand, if the Convert filter was placed
    last, then it erroneously altered the output from Slideshow Creator,
    inserting faulty html code. When that gets fixed, the problem will be
    solved the right way, but until then, to get the two filters in the
    same text format, one either has to write incomprehensibly long
    "one-liner" Slideshow Creator control blocks, or use Filterfix.

Of course the best solution is to get whatever is causing trouble with the
text filters fixed. That's long term, but you often can't control when
things get fixed, and you want your site running today. Maybe Filterfix can
help.


Skill Level:
------------

You might need to be able to find a regular expression (regex) in the PHP
code for a module to effectively use this module (or get someone else to
find it for you). No regex writing should be needed; no PHP programming is
ever necessary.

If it is Slideshow Creator that is causing you grief, the author has done
the job for you - no regex skills needed.


Task 0: Install Filterfix:
--------------------------

Insert the module code and activate the module in the usual way on the
"Modules" page.


Task 1: Setting Up The Text Format Filters:
-------------------------------------------

Filterfix consists of two text filters: "Filterfix protect", and "Filterfix
Restore". Now let's say you have two other text filters, that is, the ones
you are trying to get to "play nice" with each other. Let's call them
Filter A and Filter B.

The idea is to use the two Filterfix filters to surround one of your
problematic text filters in the Filter processing order of your text
format, and you put the second filter after the second Filterfix filter. So
you have a filter processing order like this:

+   > > > Filterfix protect vulnerable text

+   Filter A

+   < < < Filterfix restore protected text

+   Filter B

In the above case you would tell Filterfix how to recognise the code
belonging to the second text filter that is causing trouble (filter B,
above). The filters operate in this order: first Filterfix Protect removes
the troublesome text belonging to Filter B, then Filter A can operate
without messing up the text destined for Filter B. Then Filterfix Restore
replaces the material that was removed by Filterfix Protect, then filter B
can operate on its intended target text, which has not been messed with by
filter A.

Example:
--------

    In the case described above, one places the filters in this
    order:

    +   > > > Filterfix protect vulnerable text

    +   Convert line breaks into HTML (i.e. <br> and <p>)

    +   < < < Filterfix restore protected text

    +   Slideshow Creator


Task 2: Tell Filterfox how to find the text that it is to protect.
------------------------------------------------------------------

1.

Go to the Configuration - Content Authoring - Filterfix page.

2.

On that page, there is a "Search Pattern" text box:

    Search Pattern
    [                                                       ]
    The pattern to search for - example, for slideshow_creator use:
    /\[slideshow:(([\\][]]|[^]])+)\]/i

In the text box, insert the regex that will match the material you want to
protect. The page conveniently tells you what to use if you want to protect
Slideshow Creator control codes. Paste this code into the text box:

    Search Pattern
    [ /\[slideshow:(([\\][]]|[^]])+)\]/i                    ]
    The pattern to search for - example, for slideshow_creator use:
    /\[slideshow:(([\\][]]|[^]])+)\]/i

If you want to protect codes belonging to some other text filter, then you
can either ask a question in the issues queue here, or take a look in the
php of the module concerned to find the relevant regex. For example, in the
Slideshow Creator code, the correct regex is found in the file
slideshow_creator.inc here:

    // Slideshows end on a non-backslashed "]"
    if (preg_match_all(
      '/\[slideshow:(([\\\\][]]|[^]])+)\]/i', $output, $matches, PREG_SET_ORDER
    )) {

You don't have to (and shouldn't!) edit anything - just find the regex and
copy it.

3.

Below the Search Pattern text, there is a " Subpattern Index" text box,
pre-filled with the value 0:

    Subpattern Index
    [ 0   ]
    The index of the regex subpattern to store - use 0 to store it all

Unless you understand regexes, you probably just want to leave this as 0.
For those do do know regexes, this is the index of a subpattern within the
regex. If you only want the filter to remove and restore a part of the
complete match, you can change this to the subpattern index. See PHP's
regex documentation for details.

4.

Click "Save these settings." The regex details you entered will be saved,
and another pair of Search Pattern + Subpattern Index fields will appear in
case you need more than one kind of text pattern to be protected.

Task 3: Try it out
------------------

Test it on an unpublished page before releasing it on published material,
to make sure you have done what you think you have. But don't worry: this
filter doesn't change anything in your node, it only affects the output
processing. If you get it wrong, all is well - just fix it or if you don't
like Filterfix, just take it out.


To Uninstall:
-------------

Just disable the two Filterfix filters in your text formats and disable the
module.


Security:
---------

You will need to think through your usage of the module if you employ it in
any text format that can be edited by untrusted users (e.g. in site
comments etc.). Make sure that you don't allow users to "hide" dangerous
text from filters designed to detect and remove it. It might be a very bad
idea to place "Limit allowed HTML tags" or "Correct faulty and chopped off
HTML" inside the Filterfix filters!
