<?php
/**
 * @author Ron House http://drupal.org/user/171604
 * @file
 * A pair of filters to remove and then replace material that breaks other filters.
 */

/**
 * Module settings page.
 */
function _filterfix_admin() {
  // Get the patterns to filter

  $settings = variable_get('filterfix_settings', array());

  $description = t('The pattern to search for - example, for slideshow_creator use: /\[slideshow:(([\\\\][]]|[^]])+)\]/i');
  $default_value = '/\[slideshow:(([\\\\][]]|[^]])+)\]/i';

  $subpat_descr = t('The index of the regex subpattern to store - use 0 to store it all');

  // Explanation
  $form['help'] = array(
    '#markup' => t('Enter a regular expression in the Search Pattern box below. It will be scanned for by the extraction filter and all occurrences will be replaced by an unique token. Later the replacement filter will re-insert the removed material. Any other filters in between these two will be unable to mess with the material that was stored away. Enter a number in the Subpattern Index box to control the replacement. 0 will replace the entire text that was removed (i.e. that matched the regex). 1 will replace the contents of the first parenthesised subexpression, thus allowing you to use special markers around precious material, but throwing the markers themselves away in the replacement. N.B.: This filter is for trusted administrators only. Don\'t use it in text formats that will be accessible to the public, especially if the shielded filters do critical things like fix broken html or remove dangerous html.') . '<br /><br />' . t('To enter more than one pattern, enter the first, save, and a new set of empty entry fields will appear.'),
  );

  if (! isset($settings['patterns'])) {
    $settings['patterns'] = array();
  }

  // Get each pattern but ensure that it now has a numeric index
  $i = 0;
  foreach ($settings['patterns'] as $indx => $pat) {
    $form['patterns' . $i] = array(
      '#default_value' => empty($settings['patterns'][$indx]['regex']) ?
                            $default_value :
                            $settings['patterns'][$indx]['regex'],
      '#description'   => $description,
      '#title'         => t('Search Pattern'),
      '#type'          => 'textfield',
    );
    $description = t('The pattern to search for');
    $default_value = '';

    $form['subpat' . $i] = array(
      '#default_value' => empty($settings['patterns'][$indx]['subpat']) ?
                            0 : $settings['patterns'][$indx]['subpat'],
      '#description'   => $subpat_descr,
      '#title'         => t('Subpattern Index'),
      '#type'          => 'textfield',
      '#size'          => 2,
    );

    $i++;
  }

  // Add one more input field
  $form['patterns' . $i] = array(
    '#default_value' => $default_value,
    '#description'   => $description,
    '#title'         => t('Search Pattern'),
    '#type'          => 'textfield',
  );

  $form['subpat' . $i] = array(
    '#default_value' => 0,
    '#description'   => $subpat_descr,
    '#title'         => t('Subpattern Index'),
    '#type'          => 'textfield',
    '#size'          => 2,
  );

  // Submit button
  $form['submit'] = array(
    '#value' => t('Save these settings'),
    '#type'  => 'submit',
  );

  return $form;
}

function _filterfix_admin_submit(&$form, &$form_state) {
  $submissions = $form_state['values'];

  $settings = array();
  $settings['patterns'] = array();

  foreach ($submissions as $key => $value) {
    if (strncmp($key, 'patterns', 8) == 0 && $value != '') {
      $key = substr($key, 8);
      $settings['patterns'][$key] = array();
      $settings['patterns'][$key]['regex'] = $value;
      $settings['patterns'][$key]['subpat'] = 0;
    }
  }

  foreach ($submissions as $key => $value) {
    if (
      strncmp($key, 'subpat', 6) == 0 &&
      is_numeric($value) &&
      isset($settings['patterns'][$key = substr($key, 6)])
    ) {
      $settings['patterns'][$key]['subpat'] = $value;
    }
  }

  variable_set('filterfix_settings', $settings);

  drupal_set_message(t('The Filterfix settings have been saved.'));
}
